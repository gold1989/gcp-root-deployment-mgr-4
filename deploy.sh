#!/usr/bin/env bash
set -e   # set -o errexit
set -u   # set -o nounset
set -o pipefail
[ "x${DEBUG:-}" = "x" ] || set -x

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

PROJECT_ID=
ROOT_ID=
CEM_DATASET_NAME=
CEM_DEFAULT_DATASET_NAME=cem_aggregated_logs_dataset
SCA_ENABLED=

function ShowUsage
{
    echo "Usage: $0 -p PROJECT_ID -r ROOT_ID"
}

while getopts p:r:d:ch o
do  case "$o" in
  p)  PROJECT_ID="$OPTARG";;
  r)  ROOT_ID="$OPTARG";;
  d)  CEM_DATASET_NAME="$OPTARG";;
  c)  SCA_ENABLED="true";;
  [?] | h) ShowUsage ; exit 1;;
  esac
done

if [[ "${PROJECT_ID}" == "PROJECT_ID" || "${ROOT_ID}" == "ROOT_ID" ]]; then
  ShowUsage
  exit 1
fi

if [[ -z "${PROJECT_ID}" || -z "${ROOT_ID}" ]]; then
  ShowUsage
  exit 1
fi

if [[ -z "${CEM_DATASET_NAME}" ]]; then
  CEM_DATASET_NAME=${CEM_DEFAULT_DATASET_NAME}
fi

echo "Start deploying CEM resources"
sh ./cem_deploy.sh -p ${PROJECT_ID} -r ${ROOT_ID} -d ${CEM_DATASET_NAME}
if [ $? -ne 0 ]; then
  echo "An error occurred while deploying CEM resources, aborting."
  exit 1
fi
echo "Finished deploying CEM resources"

if [[ "${SCA_ENABLED}" == "true" ]]; then
    echo "Start deploying SCA resource"
    sh ./sca_deploy.sh -p ${PROJECT_ID} -r ${ROOT_ID}
    if [ $? -ne 0 ]; then
      echo "An error occurred while deploying SCA resources."
      exit 1
    fi
    echo "Finished deploying SCA resource"
fi